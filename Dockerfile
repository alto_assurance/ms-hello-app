FROM python:3.7-alpine3.9
RUN apk update \
    && apk add --update build-base git make \
    && pip install gunicorn \
    && pip install flask \
    && pip install connexion[swagger-ui]


WORKDIR /app-run
COPY . /app-run


ENTRYPOINT ["gunicorn", "-w 4", "--bind", "0.0.0.0:8000", "--access-logfile", "-", "--error-logfile", "-", "server:app"]

