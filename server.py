from flask import render_template
import connexion
from flask import jsonify


# Create the application instance
connx_app = connexion.App(__name__, specification_dir='./')

# Read the swagger.yml file to configure the endpoints
connx_app.add_api('swagger.yaml')

app = connx_app.app

# Create a URL route in our application for "/"
@app.route('/')
def home():
    """
    This function just responds to the browser ULR
    localhost:5000/
    :return:        the rendered template 'home.html'
    """
    return render_template('home.html')
